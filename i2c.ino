// set initial settings for ADC. Maybe some of these will be held in flash one day

uint8_t ADCInitSimple() {
    ll.printf ("Checking for device at 0x%02X...", i2c_addr);
  if (!checkDevice()) {
        ll.printf ("failed\n");
    return 0;
  }
    ll.printf ("found!\n");


  delay(10);
  ADCWrite (0x01, 0x01);      // reset all
  delay(10);
  ADCWrite (0x02, 0x81);      // sleep config: internal AREG and wake up
  delay(10);

//  ADCWrite (0x02, 0x81);      // sleep config: internal AREG and wake up again!
//  delay(30);

  ADCWrite (0x3B, 0x00);      // MICBIAS=VREF
//  ADCWrite (0x3C, 0x00);      // channel 1 mic, diff input, AC couple, 2.5kohm imp
  ADCWrite (0x3C, 0x24);      // channel 1 mic, single end input, AC couple, 2.5kohm imp
  ADCWrite (0x41, 0x04);      // channel 2 mic, diff input, AC couple, 10kohm imp
  ADCWrite (0x42, 80);        // channel 2 gain 0.5dB steps, so this number divided by 2 is the actual gain
  ADCWrite (0x0B, 00);        // channel 1 -> i2s slot 0 left
  ADCWrite (0x0C, 32);        // channel 2 -> i2s slot 0 right
  ADCWrite (0x07, B01110000); // i2s mode, 32 bit, normal clock polarity
  ADCWrite (0x75, 0xE0);      // Power config: MICBIAS on, ADC on, PLL on
  ADCWrite (0x73, 0xC0);      // enable channel 1 and 2 input
  ADCWrite (0x74, 0xC0);      // enable channel 1 and 2 output
  delay(30);
  return 1;
}

// convert a floating point gain to the TLV register format: a main gain (in 0.5dB increments)
// and a calibration gain (in 0.1dB increments)
void setGain (float g) {
  uint8_t whole, frac;

  if (g < 0 || g > 40) return;          // safety
  float rg = round(g * 10) / 10;        // round to nearest 0.1
  whole = rg * 2;                       // 1 is a 0.5dB step in TLV gain reg
  frac = round(fmod(rg, 0.5) * 10);     // get the leftover cal amount. 1 = 0.1dB step, hence *10
  frac += 8;                            // offset. 8 in TLV cal reg means 0dB

  whole <<= 1;                          // main gain is in bits 1-7
  frac <<= 4;                           // cal gain is in bits 4-7

  ADCWrite (CH1_CFG1_ADDRESS, whole);   // store em
  ADCWrite (CH1_CFG3_ADDRESS, frac);  
  gainChannel1=rg;
  preferences.putFloat ("gain", rg);
}

// read the gain from the ADC registers and return a float
void getGain () {
  uint8_t whole = ADCRead (CH1_CFG1_ADDRESS);   // gain in .5dB steps
  whole >>= 1;
  uint8_t frac = ADCRead (CH1_CFG3_ADDRESS);    // calibration gain in .1dB steps
  frac >>= 4;

  float theGain = ((float)whole / 2) + (0.1 * ((float)frac - 8));
  gainChannel1=theGain;
}

uint8_t ADCRead(uint8_t reg)
{
  uint8_t val;
  Wire.beginTransmission(i2c_addr);
  Wire.write(reg);
  if (Wire.endTransmission(false) != 0) return 0;
  if (Wire.requestFrom(i2c_addr, (uint8_t)2) == 0) return 0;        // cast to stop compiler complaining about literal
  val = Wire.read();
  return val;
}

bool ADCWrite(uint8_t reg, uint8_t val)
{
  Wire.beginTransmission(i2c_addr);
  //  Wire.write(reg >> 8);
  Wire.write(reg);
  //  Wire.write(val >> 8);
  Wire.write(val);
  if (Wire.endTransmission() == 0) return true;
  return false;
}

uint8_t ADCModify(uint8_t reg, uint8_t val, uint8_t iMask)
{
  uint8_t val1 = (ADCRead(reg) & (~iMask)) | val;
  if (!ADCWrite(reg, val1)) return 0;
  return val1;
}

bool checkDevice() {
  Wire.beginTransmission(i2c_addr);
  byte error = Wire.endTransmission();
  if (error == 0) return true;
  return false;
}

void printDeviceStatus () {
  uint8_t stat = ADCRead (0x77);
  ll.printf ("0x%02X: ", stat);
  stat = stat >> 5;
  switch (stat) {
    case 4: ll.print ("sleeping"); break;
    case 6: ll.print ("active mode, no channels active (no LRCLK?) "); break;
    case 7: ll.print ("active mode, channels active"); break;
  }
  ll.printf ("\n");
}
