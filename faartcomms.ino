void processCommand() {
  if (ctrlBuf[0] == '@') {
    String m[2];
//    ll.printf ("buff=%s\n", ctrlBuf.c_str());
    StringSplit(ctrlBuf.substring(1, ctrlBuf.length()), '=', m, 2);
//    ll.printf ("command: [%s], value=%s\n", m[0].c_str(), m[1].c_str() );
    if (m[0] == "inputsel") {
      inputChannel= (m[1].toInt());
    } else if (m[0] == "dbref") {
      float newref = m[1].toFloat();
      ll.printf ("setting current level ref to %.2fdBZ SPL\n",newref);
      raw_ref = temp_peak_raw ;
      db_ref = newref;
      // store em in preferences
      preferences.putFloat ("raw_ref", raw_ref);
      preferences.putFloat ("db_ref", db_ref);
    } else if (m[0] == "gain") {
      setGain(m[1].toFloat());
    } else if (m[0] == "reset") {
      ESP.restart();
    } else if (m[0] == "resetleq") {
      resetLeq();
    } else if (m[0] == "coeff") {
    
      // new mic filter coeffs
      String co[9];
      double dco[9];
//      ll.printf("%s\n\n",m[1].c_str());
      // ugh
      StringSplit(m[1], ',', co, 9);
      // convert em all to double
      for (int i=0; i<9; i++) {
        dco[i] = co[i].toDouble();
        ll.printf ("%i->%.16f\n",i, dco[i]);
      }
      // install
      installCoeff (dco);

      
    }
  }
  ctrlBuf = "";
}

void installCoeff(double *co){

 //install coeffs
  SOS_Coefficients s1 = { co[1], co[2], co[3], co[4]};
  SOS_Coefficients s2 = { co[5], co[6], co[7], co[8]};
  custom.sos[0] = s1;
  custom.sos[1] = s2;
    custom.num_sos=2;
  custom.gain=co[0];
}
